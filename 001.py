import re

def change_sentence(str1):
    str = re.sub('\W', '',str1.lower())
    if str == str[::-1]:
        print(str1, "merupakan Palindrome")
    else:
        print(str1, " merupakan Bukan Palindrome")

change_sentence("ibu ratna antar ubi")
change_sentence("kasur ini rusak")
change_sentence("A nut for a jar of tuna.")
change_sentence("Borrow or rob?")
change_sentence("Was it a car or a cat I saw?")
change_sentence("Yo, banana boy!")
change_sentence("UFO tofu?")