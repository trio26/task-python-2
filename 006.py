import random, string


class Str:
    def lower(str):
        return print(str.lower())
    def upper(str):
        return str.upper()
    def capitalize(str):
        return print(str.capitalize())
    def reverse(str):
        return print(str[::-1])
    def contains(str1, str2):
        return print(str2 in str1)
    def random(strLength=16):
        randomString = string.ascii_lowercase
        return print(''.join(random.choice(randomString) for i in range(strLength)))
    def count(str1):
        return print(len(str1))
    def countWords(str1):
        wordLength = len(str1.split())
        return print(str(wordLength))
    def trim(text,length=100):
        return print(text[:length],"***")
    def trimWords(text,words=30):
        return print(' '.join(text.split()[:words]),"***")

Str.lower("MAKAN")
Str.upper("malam")
Str.capitalize("saya ingin makan")
Str.reverse("kasur")
Str.contains('Saya ingin makan sate', 'makan')
Str.contains('Saya ingin makan sate', 'rujak')
Str.random(32)
Str.count("lorem ipsum")
Str.countWords("lorem ipsum")
text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
Str.trim(text,20)
Str.trimWords(text,3)



        