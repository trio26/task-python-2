import re



strings = """ Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. """

words = ["dolor", "elit", "quis", "nisi", "fugiat", "proident", "laborum"]
# method sub

strings = re.sub(r"[,\.$]","",strings)

list_data = strings.split()
new_data = []

for i in list_data:
    if i in words:
        asterisk = "*"*len(i)
        new_data.append(asterisk)
    else:
        new_data.append(i)

print(" ".join(new_data))
